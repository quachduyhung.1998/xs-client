<div class="grid md:grid-cols-10 grid-cols-5 gap-3">
    <?php for($i = $from; $i <= $to; $i++) : ?>
    <?php
        $randNumber = rand(1, 20);
        $numberDisplay = $i < 10 ? "00$i" : ($i < 100 ? "0$i" : $i);
    ?>
    <div class="relative bg-card-color text-center rounded-lg px-3 py-5 cursor-pointer duration-100 group hover:bg-main-color hover:text-white">
        <span><?php $numberDisplay ?></span>
        <span data-tooltip-target="tooltip__<?php $nameClass ?? '' ?>__<?php $i ?>" data-tooltip-placement="top" class="absolute top-0 right-0 text-white text-xs px-1 rounded-bl-md rounded-tr-lg <?php $randNumber <= 5 ? 'bg-gray-500' : ($randNumber <= 9 ? 'bg-main-color-ball-2' : 'bg-red-600') ?>"><?php $randNumber ?></span>
        <div id="tooltip__<?php $nameClass ?? '' ?>__<?php $i ?>" role="tooltip" class="absolute z-10 invisible inline-block px-3 py-2 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 tooltip">
            Số <?php $numberDisplay ?> đã có <?php $randNumber ?> lần không xuất hiện trong 50 kỳ quay gần nhất.
            <div class="tooltip-arrow" data-popper-arrow></div>
        </div>
    </div>
    <?php endfor; ?>
</div>
